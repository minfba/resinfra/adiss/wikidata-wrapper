package de.unibamberg.gtf.services.wikidata.processing;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.opencsv.CSVReader;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PropertyConfig {

    @Getter
    @Setter
    private String propertyName;

    @Getter
    @Setter
    private boolean considerAllStatements;
    
    @Getter
    @Setter
    private boolean considerQualifiers;


    public static Map<String, PropertyConfig> readPropertyConfig(String path) {
        Map<String, PropertyConfig> propertyMap = new HashMap<>();
        try (CSVReader reader = new CSVReader(new FileReader(path))) {
            List<String[]> csvData = reader.readAll();

            for (String[] row : csvData) {
                PropertyConfig propertyConfig = new PropertyConfig();

                propertyConfig.setPropertyName(row[0]);

                int stmts = Integer.parseInt(row[1]);
                if (stmts == 1)
                    propertyConfig.setConsiderAllStatements(true);
                else 
                    propertyConfig.setConsiderAllStatements(false);
                
                int qualifiers = Integer.parseInt(row[2]);
                if (qualifiers == 1)
                    propertyConfig.setConsiderQualifiers(true);
                else 
                    propertyConfig.setConsiderQualifiers(false);

                propertyMap.put(row[0], propertyConfig);
            }
        } catch (IOException e) {
            log.error("path", e);
        }
        return propertyMap;
    }   

}