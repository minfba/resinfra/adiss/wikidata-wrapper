package de.unibamberg.gtf.services.wikidata.processing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import de.unibamberg.minf.core.wrapper.mapping.Mapping;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EntityProcessor {


    private Map<String, String> entityCache;
    private Gson gson;

    public EntityProcessor() {
        entityCache = new HashMap();
        gson = new Gson();
    }

    public String getEntity(String key) {
        synchronized (entityCache) {
            return entityCache.get(key);
        }
    }

    public void setEntity(String key, String value) {
        synchronized (entityCache) {
            entityCache.put(key, value);
        }
    }
    
    public String resolveEntity(String entityId) {
        String name = getEntity(entityId);

        if (name == null) {
            // get from wikidata
            Map<String, Object> propertyMap = getEntityFromWikidata(entityId);
            if (propertyMap != null) {
                // cache value
                //log.info("Resolve entity " + entityId + " from wikidata");
                name = (String) Mapping.GetValueByPath(propertyMap, new String[]{"entities", entityId, "labels", "en", "value"});
                //name = (String)((Map)((Map)((Map)((Map)propertyMap.get("entities")).get(entityId)).get("labels")).get("en")).get("value");
                if (name == null)
                    name = entityId;
                setEntity(entityId, name);
            } else 
                name = entityId;
        } 

        return name;
    }

    public Map<String, Object> getEntityFromWikidata(String entityId) {
        try {
            URL url = new URL("https://www.wikidata.org/wiki/Special:EntityData/"+entityId+".json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                String json = response.toString();
                
                Map<String, Object> map = gson.fromJson(json, HashMap.class);
                return map;
            } else {
                log.error("Failed to fetch data. Response code: " + responseCode);
                return null;
            }
        } catch (IOException e) {
            log.error("Could not resolve Property " + entityId + ":\n" + e);
            
        }
        return null;
    }

}
