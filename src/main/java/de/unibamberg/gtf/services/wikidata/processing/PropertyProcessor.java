package de.unibamberg.gtf.services.wikidata.processing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class PropertyProcessor {
    
    private Map<String, String> propertyCache;
    private Gson gson;

    public PropertyProcessor() {
        propertyCache = new HashMap();
        gson = new Gson();        
    }

    public String getProperty(String key) {
        synchronized (propertyCache) {
            return propertyCache.get(key);
        }
    }

    public void setProperty(String key, String value) {
        synchronized (propertyCache) {
            propertyCache.put(key, value);
        }
    }

    public String resolveProperty(String propertyId) {
        String name = getProperty(propertyId);

        if (name == null) {
            // get from wikidata
            Map<String, Object> propertyMap = getPropertyFromWikidata(propertyId);
            if (propertyMap != null) {
                // cache value
                name = (String)((Map)((Map)((Map)((Map)propertyMap.get("entities")).get(propertyId)).get("labels")).get("en")).get("value");
                setProperty(propertyId, name);
            } else 
                name = propertyId;
        } 
        return name;
    }

    public Map<String, Object> getPropertyFromWikidata(String propertyId) {
        try {
            URL url = new URL("https://www.wikidata.org/wiki/Special:EntityData/"+propertyId+".json");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                String json = response.toString();
                
                Map<String, Object> map = gson.fromJson(json, HashMap.class);
                return map;
            } else {
                log.error("Failed to fetch data. Response code: " + responseCode);
                return null;
            }
        } catch (IOException e) {
            log.error("Could not resolve Property " + propertyId + ":\n" + e);
            
        }
        return null;
    }


    public static void main(String[] args) {
        PropertyProcessor propertyProcessor = new PropertyProcessor();
        propertyProcessor.resolveProperty("P214");
    }

}
