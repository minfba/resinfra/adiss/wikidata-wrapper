package de.unibamberg.gtf.services.wikidata.processing.geo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.core.wrapper.mapping.Mapping;


@Component
public class GeoJsonPolygonRetriever {


    public static String GEOJSON_SUBDIVISION_URL = "https://datahub.io/core/geo-ne-admin1/r/admin1.geojson";
    public static String GEOJSON_COUNTRY_URL = "https://datahub.io/core/geo-countries/r/countries.geojson";
    public static String GEOJSON_SUBDIVISION_FILE = "data/polygons/geodata/subdivsion_polygons.json";
    public static String GEOJSON_COUNTRY_FILE = "data/polygons/geodata/country_polygons.json";

    private List<Map<String, Object>> features; 
    private Map<String, Map<String, Object>> coordinateMap;

    public GeoJsonPolygonRetriever() throws StreamReadException, DatabindException, IOException {
        loadPolygons();
    }

    public static void Download() throws FileNotFoundException, MalformedURLException, IOException {
        if(!new File(GEOJSON_SUBDIVISION_FILE).exists())
            new FileOutputStream(GEOJSON_SUBDIVISION_FILE).getChannel().transferFrom(Channels.newChannel(new URL(GEOJSON_SUBDIVISION_URL).openStream()), 0, Long.MAX_VALUE);
        if(!new File(GEOJSON_COUNTRY_FILE).exists())
            new FileOutputStream(GEOJSON_COUNTRY_FILE).getChannel().transferFrom(Channels.newChannel(new URL(GEOJSON_COUNTRY_URL).openStream()), 0, Long.MAX_VALUE);
    }

    public void loadPolygons() throws StreamReadException, DatabindException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        coordinateMap = new HashMap<>();
        Map<String, Object> geojsonMap = new HashMap<>();

        Resource subdivisionResource = new ClassPathResource(GEOJSON_SUBDIVISION_FILE);
        Resource countryResource = new ClassPathResource(GEOJSON_COUNTRY_FILE);

        features = (List<Map<String, Object>>) mapper.readValue(subdivisionResource.getInputStream(), new TypeReference<Map<String, Object>>() {}).get("features");
        for (Map m : features) {
            geojsonMap.clear();
            String name = (String) ((Map) m.get("properties")).get("name");
            geojsonMap.put("type", (String) Mapping.GetValueByPath(m, new String[]{"geometry", "type"}));
            geojsonMap.put("coordinates", Mapping.GetValueByPath(m, new String[]{"geometry", "coordinates"}));
            coordinateMap.put(name, geojsonMap);
        }

        features = (List<Map<String, Object>>) mapper.readValue(countryResource.getInputStream(), new TypeReference<Map<String, Object>>() {}).get("features");
        for (Map m : features) {
            geojsonMap.clear();
            String name = (String) ((Map) m.get("properties")).get("ADMIN");
            geojsonMap.put("type", (String) Mapping.GetValueByPath(m, new String[]{"geometry", "type"}));
            geojsonMap.put("coordinates", Mapping.GetValueByPath(m, new String[]{"geometry", "coordinates"}));
            coordinateMap.put(name, geojsonMap);
        }
    }   

    public Map<String, Object> getPolygonByName(String name) {
        return coordinateMap.get(name);
    }



}
