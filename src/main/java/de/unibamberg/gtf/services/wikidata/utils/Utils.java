package de.unibamberg.gtf.services.wikidata.utils;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import de.unibamberg.minf.core.wrapper.utils.DownloadUtils;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Slf4j
@Component
public class Utils {
	
	public static File getDownloadFilePath(String fileName) {
		File dir = getDownloadDir();
		return new File(dir, fileName);
	}
	
	public static File getDownloadDir() {
		String filePath = System.getProperty("java.io.tmpdir");
		if (!filePath.endsWith(File.separator)) {
			filePath = filePath + File.separator;
		}
		return new File(filePath);
	} 

    public static <T> T[] concatWithCollection(T[] array1, T[] array2) {
        List<T> resultList = new ArrayList<>(array1.length + array2.length);
        Collections.addAll(resultList, array1);
        Collections.addAll(resultList, array2);

        @SuppressWarnings("unchecked")
        //the type cast is safe as the array1 has the type T[]
        T[] resultArray = (T[]) Array.newInstance(array1.getClass().getComponentType(), 0);
        return resultList.toArray(resultArray);
    }

    public static void unGzipFile(String compressedFile, String decompressedFile) {

        byte[] buffer = new byte[1024];

        try {

            FileInputStream fileIn = new FileInputStream(compressedFile);

            InflaterInputStream inputStream = new GZIPInputStream(fileIn);

            FileOutputStream fileOutputStream = new FileOutputStream(decompressedFile);

            int bytes_read;

            while ((bytes_read = inputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, bytes_read);
            }

            inputStream.close();
            fileOutputStream.close();

            log.info("The file was decompressed successfully!");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void unzipBz2(String compressedFile, String decompressedFile) throws IOException {
        FileInputStream fin = new FileInputStream(compressedFile);
        FileOutputStream fout = new FileOutputStream(decompressedFile);
       

        BufferedInputStream in = new BufferedInputStream(fin);
        BufferedOutputStream out = new BufferedOutputStream(fout);
        
        BZip2CompressorInputStream bzIn = new BZip2CompressorInputStream(in);
        
        
        byte[] buffer = new byte[1024];
        int n;
        while ((n = bzIn.read(buffer)) > 0) {
            out.write(buffer, 0, n);
        }
        out.flush();
        log.info("File unzipped successfully!");
        
        fin.close();
        in.close();
        fout.close();
        out.close();
        bzIn.close();
    }

    public static void decompressTarGZ(String compressedFile, String decompressedFile) throws FileNotFoundException, IOException {
        try (InputStream fileInputStream = new FileInputStream(compressedFile);
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
             GzipCompressorInputStream gzipInputStream = new GzipCompressorInputStream(bufferedInputStream);
             TarArchiveInputStream tarInputStream = new TarArchiveInputStream(gzipInputStream)) {

            ArchiveEntry entry;
            while ((entry = tarInputStream.getNextEntry()) != null) {
                if (!tarInputStream.canReadEntryData(entry)) {
                    continue;
                }

                File outputFile = new File(decompressedFile, entry.getName());
                if (entry.isDirectory()) {
                    if (!outputFile.exists() && !outputFile.mkdirs()) {
                        throw new IOException("Failed to create directory: " + outputFile);
                    }
                } else {
                    try (OutputStream outputFileStream = new FileOutputStream(outputFile);
                         BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputFileStream)) {
                        byte[] buffer = new byte[1024];
                        int len;
                        while ((len = tarInputStream.read(buffer)) != -1) {
                            bufferedOutputStream.write(buffer, 0, len);
                        }
                    }
                }
            }
        }
    }

    public static void unZipFiles(String compressedFile) throws IOException {
        File destDir = new File("./");
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(compressedFile));
        ZipEntry zipEntry = zis.getNextEntry();

        while (zipEntry != null) {
            File newFile = newFile(destDir, zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs())
                    throw new IOException("Failed to create directory " + newFile);
            } else {
                // fix for Windows-created archives
                File parent = newFile.getParentFile();
                if (!parent.isDirectory() && !parent.mkdirs()) {
                    throw new IOException("Failed to create directory " + parent);
                }

                // write file content
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();
    }

    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }


    public static <K, V extends Comparable<? super V>> Map<K, V> sortMapByValue(Map<K, V> map, boolean reverse) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        if (reverse)
            Collections.reverse(list);

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
    
    public static boolean stringContainsItemFromList(String inputStr, String[] items) {
        return Arrays.stream(items).anyMatch(inputStr::contains);
    }
    

    public static List<String> readPropertyIDsFromResources(String filePath) throws IOException {
        List<String> lines = new ArrayList<>();

        ClassPathResource resource = new ClassPathResource(filePath);
        try (InputStream inputStream = resource.getInputStream()) {
            // Read the file content into a string
            String fileContent = IOUtils.toString(inputStream, StandardCharsets.UTF_8);

            // Split the content into lines
            String[] fileLines = fileContent.split("\\r?\\n");

            // Add each line to the list
            for (String line : fileLines) {
                lines.add(line);
            }
        }

        return lines;
    }
    

}
