package de.unibamberg.gtf.services.wikidata;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.wikidata.wdtk.datamodel.interfaces.EntityDocumentProcessor;
import org.wikidata.wdtk.dumpfiles.DumpProcessingController;
import org.wikidata.wdtk.dumpfiles.MwDumpFile;
import org.wikidata.wdtk.dumpfiles.MwLocalDumpFile;

import de.unibamberg.gtf.services.wikidata.utils.DataDownloader;
import de.unibamberg.gtf.services.wikidata.wrappers.WikidataWrapperImpl;
import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class WikidatawrapperApplication implements CommandLineRunner {

	@Autowired
	private CommandLineArguments commandLineArguments;

	@Autowired
	private WikidataWrapperImpl wikidataWrapperImpl;

	@Override
	public void run(String... args) throws Exception {
		System.setProperty("jdk.xml.totalEntitySizeLimit", String.valueOf(Integer.MAX_VALUE));

		String dumpPath = Paths.get("").toAbsolutePath().toString(); // Current directory
		String mapping = null; // No mapping file by default
		String propertyConfig = null;
		int numThreads = 2;
		int bufferSize = 1000;
		String dumpFilePath = null;

		// Parse arguments
		for (String arg : args) {
			log.info("Argument: {}", arg);
			if (arg.startsWith("--path=")) {
				dumpPath = arg.substring("--path=".length());
			} else if (arg.startsWith("--mapping=")) {
				mapping = arg.substring("--mapping=".length());
			} else if (arg.startsWith("--propertyConfig=")) {
				propertyConfig = arg.substring("--propertyConfig=".length());
			} else if (arg.startsWith("--numThreads=")) {
				numThreads = Integer.parseInt(arg.substring("--numThreads=".length()));
			} else if (arg.startsWith("--bufferSize=")) {
				bufferSize = Integer.parseInt(arg.substring("--bufferSize=".length()));
			} else if (arg.startsWith("--dumpFile=")) {
				dumpFilePath = arg.substring("--dumpFile=".length());
			}
		}

		// Log the parsed values
		log.info("Path: {}", dumpPath);
		log.info("Mapping: {}", mapping == null ? "None" : mapping);
		log.info("PropertyConfig: {}", propertyConfig == null ? "None" : propertyConfig);
		log.info("NumThreads: {}", numThreads);
		log.info("BufferSize: {}", bufferSize);
		log.info("DumpFilePath: {}", dumpFilePath == null ? "None" : dumpFilePath);

		if (mapping == null || mapping.isEmpty()) {
			log.error("Mapping must be given");
			return;
		}
		if (propertyConfig == null || propertyConfig.isEmpty()) {
			log.error("PropertyConfig must be given");
			return;
		}

		commandLineArguments.setDumpPath(dumpPath);
		commandLineArguments.setMappingPath(mapping);
		commandLineArguments.setPropertyConfigPath(propertyConfig);
		commandLineArguments.setNumThreads(numThreads);
		commandLineArguments.setBufferSize(bufferSize);

		// WikidataWrapperImpl wrapper = new WikidataWrapperImpl(dumpPath, mapping,
		// propertyConfig, numThreads,
		// bufferSize);

		if (dumpFilePath != null && !dumpFilePath.isEmpty()) {
			processDumpFile(dumpFilePath);
		} else {
			String regex = "\\.xml-(.*?)\\.bz2";
			List<String> fileURLs = DataDownloader.getArticleXMLLinkList();
			for (String fileURL : fileURLs) {

				String filePath = dumpPath + "/" + fileURL.replaceAll(regex, ".xml.bz2");
				filePath = filePath.replace(DataDownloader.XML_LATEST_URL, "");
				log.info("Started Download: " + fileURL);
				DataDownloader.downloadFileFromUrl(fileURL, filePath);
				log.info("Download completed: " + filePath);

				processDumpFile(filePath);
				
				new File(filePath).delete();
			}
		}
	}

	private void processDumpFile(String dumpFilePath) {
		DumpProcessingController dumpProcessingController = new DumpProcessingController(
			"wikidatawiki");
		dumpProcessingController.setOfflineMode(true);

		wikidataWrapperImpl.open();
		dumpProcessingController.registerEntityDocumentProcessor(
				(EntityDocumentProcessor) wikidataWrapperImpl, null, true);

		MwDumpFile dumpFile = new MwLocalDumpFile(dumpFilePath);

		dumpProcessingController.processDump(dumpFile);
		// dumpProcessingController.processMostRecentJsonDump();

		wikidataWrapperImpl.close();

		log.info("Successfully processed " + wikidataWrapperImpl.getClass().getName());
	}

	public static void main(String[] args) {
        log.info("STARTING THE APPLICATION");
        
        // for debugging 
        /*String mappingArg = "--mapping=C:\\Users\\ba3ev2\\Local Folders\\My Local Files\\repos\\wikidata-wrapper\\config\\mapping.json";
        String threadArg = "--numThreads=4";
        String propertyConfigPath = "--propertyConfig=C:\\Users\\ba3ev2\\Local Folders\\My Local Files\\repos\\wikidata-wrapper\\config\\propertyConfig.csv";
        String dumpFile = "--dumpFile=C:\\Users\\ba3ev2\\Local Folders\\My Local Files\\repos\\wikidata-wrapper\\wikidatawiki-20240501-pages-articles1.xml.bz2";
		String bufferSize = "--bufferSize=1000";
		String[] arrgs = new String[5];
        arrgs[0] = mappingArg;
        arrgs[1] = threadArg;
		arrgs[2] = propertyConfigPath;
		arrgs[3] = dumpFile;
		arrgs[4] = bufferSize;
        args = arrgs;*/

        SpringApplication.run(WikidatawrapperApplication.class, args);
        log.info("APPLICATION FINISHED");
    }
}
