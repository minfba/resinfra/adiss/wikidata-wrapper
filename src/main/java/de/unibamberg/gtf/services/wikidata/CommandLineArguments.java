package de.unibamberg.gtf.services.wikidata;

import org.springframework.stereotype.Component;

import lombok.Setter;

import lombok.Getter;

@Component
public class CommandLineArguments {

    @Getter
    @Setter
    private String dumpPath;

    @Getter
    @Setter
    private String mappingPath;

    @Getter
    @Setter
    private String propertyConfigPath;

    @Getter
    @Setter
    private int numThreads;

    @Getter
    @Setter 
    private int bufferSize;

    @Getter
    @Setter
    private String dumpFilePath;

}





