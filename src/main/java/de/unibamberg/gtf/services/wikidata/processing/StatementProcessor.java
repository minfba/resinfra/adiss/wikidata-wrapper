package de.unibamberg.gtf.services.wikidata.processing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.wikidata.wdtk.datamodel.implementation.EntityIdValueImpl;
import org.wikidata.wdtk.datamodel.implementation.GlobeCoordinatesValueImpl;
import org.wikidata.wdtk.datamodel.implementation.MonolingualTextValueImpl;
import org.wikidata.wdtk.datamodel.implementation.QuantityValueImpl;
import org.wikidata.wdtk.datamodel.implementation.SomeValueSnakImpl;
import org.wikidata.wdtk.datamodel.implementation.StringValueImpl;
import org.wikidata.wdtk.datamodel.implementation.TimeValueImpl;
import org.wikidata.wdtk.datamodel.implementation.ValueSnakImpl;
import org.wikidata.wdtk.datamodel.interfaces.ItemDocument;
import org.wikidata.wdtk.datamodel.interfaces.Snak;
import org.wikidata.wdtk.datamodel.interfaces.SnakGroup;
import org.wikidata.wdtk.datamodel.interfaces.Statement;
import org.wikidata.wdtk.datamodel.interfaces.StatementGroup;
import org.wikidata.wdtk.datamodel.interfaces.Value;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.gtf.services.wikidata.CommandLineArguments;
import de.unibamberg.gtf.services.wikidata.processing.geo.FlickrShapePolygonRetriever;
import de.unibamberg.gtf.services.wikidata.processing.geo.GeoJsonPolygonRetriever;
import de.unibamberg.minf.core.wrapper.mapping.Mapping;
import de.unibamberg.minf.core.wrapper.utils.GeoUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StatementProcessor {

    @Autowired
    CommandLineArguments commandLineArguments;

    @Autowired
    PropertyProcessor propertyProcessor;

    @Autowired
    EntityProcessor entityProcessor;

    @Autowired
    FlickrShapePolygonRetriever flickrShapePolygonRetriever;

    @Autowired
    GeoJsonPolygonRetriever geoJsonPolygonRetriever;

    Map<String, PropertyConfig> propertyMap;

    private ObjectMapper mapper = new ObjectMapper();

    public void init() {
        propertyMap = PropertyConfig.readPropertyConfig(commandLineArguments.getPropertyConfigPath());
    }

    public Map<String, Object> resolveStatements(ItemDocument doc) {
        Map<String, Object> statementMap = new HashMap<>(doc.getStatementGroups().size());

        // only consider properties that are needed
        List<StatementGroup> validGroups = new ArrayList<>();
        for (String id : propertyMap.keySet()) {
            StatementGroup g = doc.findStatementGroup(id);
            if (g != null)
                validGroups.add(g);
        }

        for (StatementGroup sg : validGroups) {
            String propertyId = sg.getProperty().getId();
            PropertyConfig propertyConfig = propertyMap.get(propertyId);
            String propertyName = propertyProcessor.resolveProperty(propertyId);

            List<Statement> statements = null;
            if (sg.getBestStatements() != null && !propertyConfig.isConsiderAllStatements())
                statements = sg.getBestStatements().getStatements();
            else
                statements = sg.getStatements();

            List<Object> values = new ArrayList<>();
            for (Statement statement : statements) {
                values.add(resolveStatement(statement, propertyConfig.isConsiderQualifiers()));
            }
            if (!values.isEmpty())
                statementMap.put(propertyName, values);
        }

        resolveEntityIds(statementMap, "instance of");

        // processGeoBox(statementMap);
        // resolve coordinate location
        if (statementMap.containsKey("coordinate location")
                && statementMap.get("coordinate location") instanceof List) {
            processCoordinateLocation(statementMap);
        }

        if (statementMap.containsKey("geoshape"))
            processGeoshape(doc.findLabel("en"), statementMap);

        processISOCodes(statementMap);

        processExternalLinks(statementMap);

        return statementMap;
    }

    private Object resolveStatement(Statement statement, boolean considerQualifiers) {
        Snak mainSnak = statement.getMainSnak();

        if (mainSnak instanceof ValueSnakImpl) {

            Object value = resolveValueSnakImpl((ValueSnakImpl) mainSnak);

            if (considerQualifiers) {
                Map<String, Object> qualifiers = resolveQualifiers(statement.getQualifiers());
                if (qualifiers != null && !qualifiers.isEmpty()) {
                    qualifiers.put("value", value);
                    return qualifiers;
                } else {
                    return value;
                }
            } else {
                return value;
            }

        } else if (mainSnak instanceof SomeValueSnakImpl) {
            // TODO: impl?
        } else {
            // TODO: impl?
        }
        return null;
    }

    private Object resolveValueSnakImpl(ValueSnakImpl valueSnakImpl) {
        Value dataValue = valueSnakImpl.getDatavalue();

        if (dataValue instanceof StringValueImpl) {
            return ((StringValueImpl) dataValue).getValue();
        } else if (dataValue instanceof EntityIdValueImpl) {
            return ((EntityIdValueImpl) dataValue).getId();
        } else if (dataValue instanceof GlobeCoordinatesValueImpl) {
            GlobeCoordinatesValueImpl coordinatesValueImpl = (GlobeCoordinatesValueImpl) dataValue;
            return new double[] { coordinatesValueImpl.getLongitude(), coordinatesValueImpl.getLatitude() };
        } else if (dataValue instanceof QuantityValueImpl) {
            return resolveQuantityValues((QuantityValueImpl) dataValue);
        } else if (dataValue instanceof MonolingualTextValueImpl) {
            return (Map<String, Object>) mapper.convertValue(dataValue, Map.class);
        } else if (dataValue instanceof TimeValueImpl) {
            return ((TimeValueImpl) dataValue).toString();
        } else {
            // TODO
            return null;
        }
    }

    private Map<String, Object> resolveQualifiers(List<SnakGroup> qualifiers) {
        if (qualifiers.size() == 0) {
            return null;
        } else {
            Map<String, Object> m = new HashMap<>();
            // TODO: iterate and consider only certain qualifiers!
            SnakGroup snakgroup = qualifiers.get(0); // we only consider one qualifier for now... TODO!
            String qualifierId = snakgroup.getProperty().getId();
            List<Object> resolvedSnaks = new ArrayList<>();
            if (qualifierId.equals("P585") // Point In Time
                    || qualifierId.equals("P580") // Start time
                    || qualifierId.equals("P582")) { // End time
                if (snakgroup.getSnaks().size() > 1) {

                    for (Snak s : snakgroup.getSnaks()) {
                        if (s instanceof ValueSnakImpl)
                            resolvedSnaks.add(resolveValueSnakImpl((ValueSnakImpl) s));
                    }
                    m.put(propertyProcessor.resolveProperty(qualifierId), resolvedSnaks);
                } else {
                    if (snakgroup.getSnaks().get(0) instanceof ValueSnakImpl)
                        m.put(propertyProcessor.resolveProperty(qualifierId),
                                resolveValueSnakImpl((ValueSnakImpl) snakgroup.getSnaks().get(0)));
                }
                return m;
            }
        }
        return null;
    }

    private Object resolveQuantityValues(QuantityValueImpl dataValue) {
        return dataValue.getNumericValue();
    }

    private void processISOCodes(Map<String, Object> statements) {
        if (statements.containsKey("ISO 3166-2 code")) {
            List<Object> isoCodes = (List<Object>) statements.get("ISO 3166-2 code");
            for (Object isoCodeObj : isoCodes) {
                if (isoCodeObj == null)
                    continue;
                String countryCode = "";
                String subdivisionString = "";
                if (isoCodeObj instanceof String) {
                    // either string or hashMap with value...
                    String[] split = ((String) isoCodeObj).split("-");
                    countryCode = split[0];
                    if (split.length > 1)
                        subdivisionString = split[1];
                } else if (isoCodeObj instanceof Map) {
                    String[] split = ((String) ((Map) isoCodeObj).get("value")).split("-");
                    countryCode = split[0];
                    if (split.length > 1)
                        subdivisionString = split[1];
                } else {
                    return;
                }

                if (countryCode.length() == 2) // ISO 3166-1 ALPHA-2
                    Mapping.SetValueByPath(statements, new String[] { "ISO 3166-1 alpha-2 code" }, countryCode);
                else if (countryCode.length() == 3) // ISO 3166-1 ALPHA-2
                    Mapping.SetValueByPath(statements, new String[] { "ISO 3166-1 alpha-3 code" }, countryCode);

                if (subdivisionString.length() > 0)
                    Mapping.SetValueByPath(statements, new String[] { "subdivisionCode" }, subdivisionString);
            }
        }
    }

    private Map<String, Object> processGeoBox(Map<String, Object> statements) {
        // process coordinate box: P1332, P1333, P1334, P1335
        List east = (List) statements.remove("coordinates of easternmost point");
        List north = (List) statements.remove("coordinates of northernmost point");
        List south = (List) statements.remove("coordinates of southernmost point");
        List west = (List) statements.remove("coordinates of westernmost point");
        if (east != null && !east.isEmpty() &&
                north != null && !north.isEmpty() &&
                south != null && !south.isEmpty() &&
                west != null && !west.isEmpty()) {
            // problem here: there can be multiple values for these points!
            // even when there are "best" statements
            List<Object> boxCoords = new ArrayList<>();
            boxCoords.add(east.get(0));
            boxCoords.add(west.get(0));
            boxCoords.add(south.get(0));
            boxCoords.add(north.get(0));
            statements.put("box", boxCoords);
        }
        return statements;
    }

    private Map<String, Object> processGeoshape(String enName, Map<String, Object> statements) {
        List<Object> geoshapeNames = (List) statements.get("geoshape");
        Map geoshape = new HashMap<>();

        for (Object geoshapeNameObj : geoshapeNames) {
            String geoshapeName = "";
            if (geoshapeNameObj instanceof String) {
                geoshapeName = (String) geoshapeNameObj;
            } else if (geoshapeNameObj instanceof Map) {
                geoshapeName = (String) ((Map) geoshapeNameObj).get("value");
            }
            if (geoshapeName.isEmpty())
                continue;

            String locationName = ((String) geoshapeName).replace("Data:", "")
                    .replace(".map", "")
                    .replace("Flickr", "").trim();
            if (locationName.contains("/")) {
                String[] splits = locationName.split("/");
                locationName = splits[splits.length - 1];
            }

            Map coords = geoJsonPolygonRetriever.getPolygonByName(locationName);
            if (coords == null || coords.isEmpty())
                coords = geoJsonPolygonRetriever.getPolygonByName(enName);
            if (coords == null || coords.isEmpty())
                coords = flickrShapePolygonRetriever.getPolygonAsGeoJSONByName(locationName);
            if (coords == null || coords.isEmpty())
                coords = flickrShapePolygonRetriever.getPolygonAsGeoJSONByName(enName);

            if (coords != null && !coords.isEmpty()) {
                geoshape = coords;
                break;
            }
        }

        if (geoshape.size() > 0) {
            statements.put("geoshape", geoshape);
        } else {
            // No polygon found
            statements.remove("geoshape");
            //log.warn("No polygon found for: " + enName);
        }
        return statements;
    }

    private void processCoordinateLocation(Map<String, Object> statementMap) {
        Object latlonObj = statementMap.get("coordinate location");
        // is always list of coordinatearrays [ [12.4, 29.3] ]
        List<JSONObject> geojsonList = new ArrayList<>();
        List latlonList = (List) latlonObj;
        if (latlonList.size() > 0) {
            for (int i = 0; i < latlonList.size(); i++) {
                double[] latlon = (double[]) latlonList.get(i);
                if (latlon == null)
                    continue;
                String latlonString = latlon[1] + "," + latlon[0];
                String geojson = GeoUtils.latLonStringToGeoJson(latlonString);
                JSONObject jsonObj = null;
                try {
                    jsonObj = mapper.readValue(geojson, JSONObject.class);
                    jsonObj.remove("crs");
                } catch (IOException e) {
                    log.error(geojson, e);
                }
                geojsonList.add(jsonObj);
            }
            statementMap.put("coordinate location", geojsonList);
        }
    }

    private void resolveEntityIds(Map<String, Object> statements, String propertyName) {
        if (statements.containsKey(propertyName)) {
            List<Object> entityList = (List<Object>) statements.get(propertyName);
            for (int i = 0; i < entityList.size(); i++) {
                Object entity = entityList.get(i);
                if (entity instanceof String) {
                    entity = entityProcessor.resolveEntity((String) entity);
                    entityList.set(i, entity);
                } else if (entity instanceof Map) {
                    if (((Map) entity).containsKey("value")) {
                        ((Map) entity).replace("value",
                                entityProcessor.resolveEntity((String) ((Map) entity).get("value")));
                    }
                }
            }
        }
    }

    private void processExternalLinks(Map<String, Object> statements) {
        List<Map<String, Object>> externalLinks = new ArrayList<>();

        // gnd
        if (statements.containsKey("GND ID")) {
            externalLinks.add(createExternalLink("gnd", statements.get("GND ID"), "http://d-nb.info/gnd/"));
        }
        // orcid
        if (statements.containsKey("ORCID iD")) {
            externalLinks.add(createExternalLink("orcid", statements.get("ORCID iD"), "https://orcid.org/"));
        }
        // viaf
        if (statements.containsKey("VIAF ID")) {
            externalLinks.add(createExternalLink("viaf", statements.get("VIAF ID"), "https://viaf.org/viaf/"));
        }
        // osm
        if (statements.containsKey("OpenStreetMap relation ID")) {
            externalLinks.add(createExternalLink("osm", statements.get("OpenStreetMap relation ID"),
                    "https://www.openstreetmap.org/relation/", "relation/"));
        }
        if (statements.containsKey("OpenStreetMap way ID")) {
            externalLinks.add(createExternalLink("osm", statements.get("OpenStreetMap way ID"),
                    "https://www.openstreetmap.org/way/", "way/"));
        }
        if (statements.containsKey("OpenStreetMap node ID")) {
            externalLinks.add(createExternalLink("osm", statements.get("OpenStreetMap node ID"),
                    "https://www.openstreetmap.org/node/", "node/"));
        }
        // geonames
        if (statements.containsKey("GeoNames ID")) {
            externalLinks
                    .add(createExternalLink("geonames", statements.get("GeoNames ID"), "https://www.geonames.org/"));
        }
        // loc
        if (statements.containsKey("Library of Congress authority ID")) {
            externalLinks.add(createExternalLink("loc", statements.get("Library of Congress authority ID"),
                    "https://id.loc.gov/authorities/names/"));
        }
        // isni
        if (statements.containsKey("ISNI")) {
            externalLinks.add(createExternalLink("isni", statements.get("ISNI"), "https://isni.org/isni/"));
        }

        if (!externalLinks.isEmpty())
            statements.put("externalLinks", externalLinks);
    }

    


    private Map<String, Object> createExternalLink(String provider, Object id, String urlPrefix) {
        return createExternalLink(provider, id, urlPrefix, "");
    }

    private Map<String, Object> createExternalLink(String provider, Object id, String urlPrefix, String idPrefix) {
        Map<String, Object> m = new HashMap<>();
        m.put("provider", provider);

        if (id instanceof String) {
            m.put("id", idPrefix + id);
            m.put("url", urlPrefix + id);
        } else if (id instanceof List && ((List) id).get(0) instanceof String) {
            List<String> urls = new ArrayList<>();
            List<String> ids = new ArrayList<>();
            for (String idString : (List<String>) id) {
                urls.add(urlPrefix + idString);
                ids.add(idPrefix + idString);
            }
            m.put("id", ids);
            m.put("url", urls);
        }

        return m;
    }

}
