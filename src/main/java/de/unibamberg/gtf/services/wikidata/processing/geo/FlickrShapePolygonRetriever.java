package de.unibamberg.gtf.services.wikidata.processing.geo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.unibamberg.minf.core.wrapper.mapping.Mapping;


@Component
public class FlickrShapePolygonRetriever {
    
    private final ResourceLoader resourceLoader;

    public static String URL = "http://www.flickr.com/services/shapefiles/2.0/";
    public static String TAR_GZ_FILE = "data/polflickr_shapes_public_dataset_2.0.tar.gz";
    public static String FLICKR_DIR = "data/polygons/flickr_polygons";

    
    private Map<String, Map<String, Object>> coordinateMap;

    @Autowired
    public FlickrShapePolygonRetriever(ResourceLoader resourceLoader) throws StreamReadException, DatabindException, IOException {
        this.resourceLoader = resourceLoader;
        loadPolygons();
    }

    public static void Download() throws FileNotFoundException, MalformedURLException, IOException {
        new FileOutputStream(TAR_GZ_FILE).getChannel().transferFrom(Channels.newChannel(new URL(URL).openStream()), 0, Long.MAX_VALUE);
    }


    public void loadPolygons() throws StreamReadException, DatabindException, IOException {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver(resourceLoader);
        String locationPattern = "classpath:" + FLICKR_DIR + "/*.geojson";
        Resource[] resources = resourcePatternResolver.getResources(locationPattern);

        /*File dir = ResourceUtils.getFile("classpath:" + FLICKR_DIR);
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".geojson");
            }
        });*/
        coordinateMap = new HashMap();
        Map<String, Object> geojsonMap = new HashMap();
        for (Resource resource : resources) {
            ObjectMapper mapper = new ObjectMapper();
            List<Map<String, Object>> features = (List<Map<String, Object>>) mapper.readValue(resource.getInputStream(), new TypeReference<Map<String, Object>>() {}).get("features");
            for (Map m : features) {
                geojsonMap.clear();

                String fullname = (String) ((Map) m.get("properties")).get("label");
                String name = fullname;
                geojsonMap.put("type", (String) Mapping.GetValueByPath(m, new String[]{"geometry", "type"}));
                geojsonMap.put("coordinates", Mapping.GetValueByPath(m, new String[]{"geometry", "coordinates"}));

                coordinateMap.put(name, geojsonMap);
            }
        }
    }   

    
    public Map getPolygonAsGeoJSONByName(String nameStr) {
        return coordinateMap.get(nameStr);
    }



   

}
