package de.unibamberg.gtf.services.wikidata.processing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.wikidata.wdtk.datamodel.interfaces.ItemDocument;
import org.wikidata.wdtk.datamodel.interfaces.MonolingualTextValue;
import com.fasterxml.jackson.databind.ObjectMapper;


public class DocumentProcessor {


    public static Map<String, Object> resolveDocument(ItemDocument doc, StatementProcessor statementProcessor, ObjectMapper mapper) {
        Map<String, Object> docMap = new HashMap<String,Object>();
        docMap.put("id", doc.getEntityId().getId());
        docMap.put("siteId", doc.getEntityId().getIri());
        
        Map<String, Object> mainLabel = resolveMainLabel(doc, mapper);
        if (mainLabel != null) {
            docMap.put("mainLabel", mainLabel);
        }
        List<Map<String, Object>> labels = resolveLabels(doc, mapper);
        if (labels != null && !labels.isEmpty()) {
            docMap.put("labels", labels);
        }
        List<Map<String, Object>> descriptions = resolveDescriptions(doc, mapper);
        if (descriptions != null && !descriptions.isEmpty()) {
            docMap.put("descriptions", descriptions);
        }
        List<Map<String, Object>> aliases = resolveAliases(doc);
        if (aliases != null && !aliases.isEmpty()) {
            docMap.put("aliases", aliases);
        }
        Map<String, Object> statements = statementProcessor.resolveStatements(doc);
        if (statements != null && !statements.isEmpty()) {
            docMap.put("statements", statements);
        }
        return docMap;
    }    

    private static Map<String, Object> resolveMainLabel(ItemDocument doc, ObjectMapper mapper) {
        if (doc.getLabels().containsKey("en")) {
            Map<String, Object> m = (Map<String, Object>) mapper.convertValue(doc.getLabels().get("en"), Map.class);
            m.put("@value", m.remove("value"));
            m.put("@language", m.remove("language"));
            return m;
        } else if (doc.getLabels().containsKey("de")) {
            Map<String, Object> m = (Map<String, Object>) mapper.convertValue(doc.getLabels().get("de"), Map.class);
            m.put("@value", m.remove("value"));
            m.put("@language", m.remove("language"));
            return m;
        } else { // take the first label 
            for (Map.Entry<String, MonolingualTextValue> l : doc.getLabels().entrySet()) {
                Map<String, Object> m = (Map<String, Object>) mapper.convertValue(l.getValue(), Map.class);
                m.put("@value", m.remove("value"));
                m.put("@language", m.remove("language"));
                return m;
            }
        }

        return null;
    }

    private static List<Map<String, Object>> resolveLabels(ItemDocument doc, ObjectMapper mapper) {
        List<Map<String, Object>> labelList = new ArrayList<>(doc.getLabels().size());
        Map<String, Object> m = null;
        for (Map.Entry<String, MonolingualTextValue> l : doc.getLabels().entrySet()) {
            m = (Map<String, Object>) mapper.convertValue(l.getValue(), Map.class);
            m.put("@value", m.remove("value"));
            m.put("@language", m.remove("language"));
            labelList.add(m);
        }
        return labelList;
    }

    private static List<Map<String, Object>> resolveAliases(ItemDocument doc) {
        List<Map<String, Object>> aliasList = new ArrayList<>(doc.getAliases().size());
        HashMap<String, Object> aliasMap = new HashMap<>(2);

        for (Map.Entry<String, List<MonolingualTextValue>> a : doc.getAliases().entrySet()) {
            List<String> aList = a.getValue().stream()
                .map(MonolingualTextValue::getText)
                .collect(Collectors.toList());
            aliasMap.put("@value", aList);
            aliasMap.put("@language", a.getKey());
            aliasList.add((HashMap<String, Object>) aliasMap.clone());
        }
        return aliasList;
    }

    private static List<Map<String, Object>> resolveDescriptions(ItemDocument doc, ObjectMapper mapper) {
        List<Map<String, Object>> descriptionList = new ArrayList<>(doc.getDescriptions().size());
        Map<String, Object> m = null;
        for (Map.Entry<String, MonolingualTextValue> d : doc.getDescriptions().entrySet()) {
            m = (Map<String, Object>) mapper.convertValue(d.getValue(), Map.class);
            m.put("@value", m.remove("value"));
            m.put("@language", m.remove("language"));
            descriptionList.add(m);
        }

        return descriptionList;
    }

}
