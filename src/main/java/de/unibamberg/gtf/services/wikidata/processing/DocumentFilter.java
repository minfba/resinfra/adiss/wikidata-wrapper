package de.unibamberg.gtf.services.wikidata.processing;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;

@Component
public class DocumentFilter {

    @Autowired
    ResourceLoader resourceLoader;

    @Getter
    private Set<String> filterSet;


    public DocumentFilter() {
        filterSet = new HashSet<String>();
        // loads the filterset from resources .txt file. Entries are divided by newline
        Resource filterSetResource = new ClassPathResource("data/filterset.txt");

        try {
            InputStream is = filterSetResource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                filterSet.add(line);
            }

        } catch (IOException e) {
                e.printStackTrace();
        }

    }
    
}
