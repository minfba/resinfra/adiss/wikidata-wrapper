package de.unibamberg.gtf.services.wikidata;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;

import org.wikidata.wdtk.datamodel.interfaces.EntityDocumentProcessor;
import org.wikidata.wdtk.dumpfiles.DumpProcessingController;
import org.wikidata.wdtk.dumpfiles.MwDumpFile;
import org.wikidata.wdtk.dumpfiles.MwLocalDumpFile;

import de.unibamberg.gtf.services.wikidata.wrappers.WikidataWrapperImpl;

public class WikiDumpToJSON {


    public static void main(String[] args) throws IOException, XMLStreamException {
        String DUMP_FILE = "./wikidatawiki-20240501-pages-articles1.xml.bz2";

        WikidataWrapperImpl wrapper = new WikidataWrapperImpl();
        wrapper.open();

        // Controller object for processing dumps:
        DumpProcessingController dumpProcessingController = new DumpProcessingController(
                "wikidatawiki");
        dumpProcessingController.setOfflineMode(true);

        dumpProcessingController.registerEntityDocumentProcessor(
            (EntityDocumentProcessor) wrapper, null, true);

        MwDumpFile dumpFile = new MwLocalDumpFile(DUMP_FILE);

        dumpProcessingController.processDump(dumpFile);

        wrapper.close();
        
    }

}
