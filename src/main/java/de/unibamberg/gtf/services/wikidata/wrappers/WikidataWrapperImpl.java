package de.unibamberg.gtf.services.wikidata.wrappers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wikidata.wdtk.datamodel.implementation.ItemIdValueImpl;
import org.wikidata.wdtk.datamodel.interfaces.EntityDocumentProcessor;
import org.wikidata.wdtk.datamodel.interfaces.ItemDocument;
import org.wikidata.wdtk.datamodel.interfaces.PropertyDocument;
import org.wikidata.wdtk.datamodel.interfaces.Statement;
import org.wikidata.wdtk.datamodel.interfaces.StatementGroup;
import org.wikidata.wdtk.datamodel.interfaces.Value;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;

import de.unibamberg.gtf.services.wikidata.CommandLineArguments;
import de.unibamberg.gtf.services.wikidata.processing.DocumentFilter;
import de.unibamberg.gtf.services.wikidata.processing.DocumentProcessor;
import de.unibamberg.gtf.services.wikidata.processing.StatementProcessor;
import de.unibamberg.minf.core.wrapper.mapping.Mapping;
import de.unibamberg.minf.core.wrapper.utils.Utils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class WikidataWrapperImpl implements EntityDocumentProcessor, WikidataWrapper {

	@Autowired
	private CommandLineArguments commandLineArguments;
	
	private JsonWriter jsonWriter;

    private List<Mapping> mappings;

	private List<Map<String, Object>> documentBuffer = new ArrayList<>();
	private int bufferSize = 1000;
	private int counter = 0;
	private int filterCounter = 0;
	private int numThreads = 4;

	private ExecutorService executor;
	private Queue<ItemDocument> queueBuffer;
	private Queue<Map<String,Object>> jsonQueue;

	private ObjectMapper mapper;

	@Autowired
	private StatementProcessor statementProcessor;

	@Autowired
	private DocumentFilter documentFilter;

	private Set<String> filterSet;
	
    private String dumpPath;
	private String mappingPath;

	private String dumpName = "wikidata_dump";
	
	public WikidataWrapperImpl() throws IOException {
		mapper = new ObjectMapper();

		queueBuffer = new ConcurrentLinkedQueue<>();
		jsonQueue = new ConcurrentLinkedQueue<>();

		executor = Executors.newFixedThreadPool(numThreads);


		// TODO: funzt noch nicht
		//Set<String> siteLinks = new HashSet<>();
		//siteLinks.add("wikipedia");
		//filter.setSiteLinkFilter(siteLinks);

		//this.datamodelFilter = new DatamodelFilter(new DataObjectFactoryImpl(), filter);*/
	}

	@Override
	public void open() {
		this.dumpPath = commandLineArguments.getDumpPath();
		this.mappingPath = commandLineArguments.getMappingPath();
		this.numThreads = commandLineArguments.getNumThreads();
		this.bufferSize = commandLineArguments.getBufferSize();
		
		filterSet = documentFilter.getFilterSet();

		statementProcessor.init();

		String dumpDirPath = dumpPath + "/" + dumpName;
		try {
			Files.createDirectories(Paths.get(dumpDirPath));
		} catch (IOException e) {
			log.error("Could not create dump directory", e);
			return;
		}
		
		File directory = new File(dumpDirPath);
        File[] files = directory.listFiles();
        int jsonFileCount = 0;

        if (files != null) {
            for (File file : files) {
                if (file.isFile() && file.getName().endsWith(".json")) {
                    jsonFileCount++;
                }
            }
		}

        Writer fstream = null;
        try {
			fstream = new OutputStreamWriter(new FileOutputStream(new File(dumpPath + "/" + dumpName + "/" + dumpName + jsonFileCount + "_" + Utils.GetTimestamp() + ".json")), StandardCharsets.UTF_8);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

        try {
			jsonWriter = new JsonWriter(fstream);
			jsonWriter.beginArray();
		} catch (IOException e) {
			log.error("JSON Writer could not be loaded", e);
		}

		// put path int application.properties 
        mappings = Mapping.GetMappingsFromJson(mappingPath);
	}
	
	@Override
	public void close() {
		//executor.shutdown();

		if (documentBuffer.size() > 0)
			processItemDocuments();

		try {
			jsonWriter.endArray();
			jsonWriter.flush();
			jsonWriter.close();
		} catch (IOException e) {
			log.error("Could not close json file", e);
		}
	}
	
	@Override
	public void processItemDocument(ItemDocument itemDocument) {
		if (includeDocument(itemDocument)) {
			
			/* 
			Map<String,Object> values = DocumentProcessor.resolveDocument(itemDocument, statementProcessor, mapper);

			documentBuffer.add(values);
			
			if (documentBuffer.size() >= bufferSize) {
				dumpBufferToJson();
			}
			*/

			queueBuffer.add(itemDocument);

			if (queueBuffer.size() == bufferSize) {
				processItemDocuments();
			}	

		}
	}

	private final Runnable task = () -> {
		while (!queueBuffer.isEmpty()) {
			ItemDocument doc = queueBuffer.poll();
			if (doc != null) {
				Map<String, Object> payload = DocumentProcessor.resolveDocument(doc, statementProcessor, mapper);
				String id = (String) payload.get("id");
				payload = (Map<String, Object>) Mapping.ReMap(payload, mappings);
				
				Map<String, Object> values = new HashMap<>();
				values.put("_id", id);
				values.put("payload", payload);
				jsonQueue.add(values);
			}
		}
	};

	private void processItemDocuments() {
        // Submit the tasks to the executor
        List<Future<?>> futures = new ArrayList<>();
        for (int i = 0; i < numThreads; i++) {
            futures.add(executor.submit(task));
        }

        // Wait for all tasks to complete
        for (Future<?> future : futures) {
            try {
                future.get();
            } catch (ExecutionException e) {
                log.error("error waiting for futures to complete", e);
            } catch (InterruptedException e) {
				log.error("error waiting for futures to complete", e);
			}
        }

		while (jsonQueue.size() > 0) {
			Map<String, Object> values = jsonQueue.poll();
			try {
				String s  = mapper.writeValueAsString(values);
				jsonWriter.jsonValue(s);
				counter++;
			} catch (IOException e) {
				log.error("Could not get id as String", e);
			}
		}
		log.info("Wrote " + counter + " & filtered " + filterCounter + " documents");
	}

	@Override
	public void processPropertyDocument(PropertyDocument propertyDocument) {
	}

	/**
	 * Returns true if the given document should be included in the
	 * serialization.
	 *
	 * @param itemDocument the document to check
	 * @return true if the document should be serialized
	 */
	private boolean includeDocument(ItemDocument itemDocument) {
		// check if Document contains coordinate location
		//if (itemDocument.hasStatement("P625") && 
		//	!itemDocument.findStatementGroup("P625").getStatements().isEmpty()) // should have a value
		//	return true;

		StatementGroup sg = itemDocument.findStatementGroup("P31");
		if (sg == null)
			return true;	

		
		List<Statement> statements = sg.getStatements();
		for (Statement statement : statements) {
			Value value = statement.getValue();
			if (value instanceof ItemIdValueImpl) {
				String val = ((ItemIdValueImpl)value).getId();
				if (filterSet.contains(val)) {
					filterCounter++;
					return false;
				}
			}
		}
		return true;
    }
}
