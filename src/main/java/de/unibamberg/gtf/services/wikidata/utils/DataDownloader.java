package de.unibamberg.gtf.services.wikidata.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataDownloader {
    
    public static final String XML_LATEST_URL = "https://dumps.wikimedia.org/wikidatawiki/20240501/";

    public static List<String> getArticleXMLLinkList() {
        List<String> linkUrls = new ArrayList<>();
    
        String regex = "wikidatawiki-\\d{8}-pages-articles\\d+\\.xml-p\\d+p\\d+\\.bz2";
        Pattern pattern = Pattern.compile(regex);
        try {
            URL url = new URL(XML_LATEST_URL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
            connection.setRequestMethod("GET");

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            Matcher matcher;
            while ((line = reader.readLine()) != null) {
                
                if (line.contains("<a href=")) {
                    
                    //String link = line.split("<a href=\"")[1].split("\"")[0];
                    
                    matcher = pattern.matcher(line);
                    if(matcher.find())
                        linkUrls.add(XML_LATEST_URL + matcher.group());
                }
            }
            reader.close();
            connection.disconnect();
        } catch (IOException e) {
            log.error(e.toString());
        }
        return linkUrls;
    }

    public static final boolean downloadFileFromUrl(String urlString, String filePath) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());
            long fileSize = httpConnection.getContentLength();

            try (BufferedInputStream in = new BufferedInputStream(url.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
                byte dataBuffer[] = new byte[1024];
                int bytesRead;
                long totalBytesRead = 0;

                while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                    fileOutputStream.write(dataBuffer, 0, bytesRead);
                    totalBytesRead += bytesRead;
                    //double progress = (double) totalBytesRead / fileSize * 100;
                }
            } 
            
        } catch (IOException e) {
            log.error(e.toString());
            return false;
        }
        return true;
    }

    
    public static void main(String[] args) {
        List<String> links = new DataDownloader().getArticleXMLLinkList();
        System.out.println(links);
    }

}
